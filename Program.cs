﻿using Tenduke.Core.Config;
using Tenduke.Scale.Builder;
using Tenduke.Scale.LicenseCheckout;
using Tenduke.Scale.Licensing;

string LICENSING_API_URL = "https://{YOUR_ID}.dev.lic-api.scale.10duke.com:443";

string PRODUCT_NAME = "YOUR PRODUCT";
string LICENSE_KEY = "YOUR LICENSE KEY";

string HARDWARE_ID = "YOUR-MACHINE-NAME-0015b875-ff28-4720-8a86-b385896b4865";
string? leaseId;

Console.WriteLine("Welcome! 10Duke Scale SDK License Key Demo");

TendukeConfig config = new() { LicensingApiUrl = new Uri(LICENSING_API_URL), };

LicenseCheckoutClientLicenseKeyBuilder builder = new(config, "10DukeScaleSDKLicenseKeyDemo/1.0");

var (client, _) = builder.Build();

var checkoutResult = await client.CheckoutAsync(
    LICENSE_KEY,
    [
        new LicenseCheckoutArguments
        {
            ProductName = PRODUCT_NAME,
            Quantity = 1,
            QuantityDimension = QuantityDimension.Seats
        }
    ],
    new ClientDetails { HardwareId = HARDWARE_ID }
);

var checkout = checkoutResult.First().LicenseToken!;
leaseId = checkout.LeaseId;

Console.WriteLine($"Checked out {checkout.ProductName}, with lease id: {leaseId}");

async Task Heartbeat()
{
    var heartbeatResult = await client.HeartbeatAsync(
        LICENSE_KEY,
        [new LicenseHeartbeatArguments { LeaseId = leaseId! }]
    );
    var heartbeat = heartbeatResult.First().LicenseToken!;
    Console.WriteLine(
        $"Heartbeat completed, old lease id: {heartbeat.OldLeaseId}, new lease id: {heartbeat.LeaseId}, Not before: {heartbeat.NotBefore}"
    );
    leaseId = heartbeat.LeaseId;
}

async Task Release()
{
    var releaseResult = await client!.ReleaseAsync(
        LICENSE_KEY,
        [new LicenseReleaseArguments { LeaseId = leaseId! }]
    );

    var release = releaseResult.First();
    checkout = null;
    Console.WriteLine($"Released license, status: {release.Released}");
}

while (true)
{
    Console.WriteLine("Next?");
    if (checkout != null)
    {
        Console.WriteLine("1: Heartbeat");
        Console.WriteLine("2: Release");
    }
    else
    {
        Console.WriteLine("1 + 2: Ignored - no current checkout");
    }
    Console.WriteLine("3: Exit");
    Console.Write("[3]?:");
    var command = Console.ReadLine();

    switch (command)
    {
        case "1":
            await Heartbeat();
            break;
        case "2":
            await Release();
            break;
        default:
            Environment.Exit(0);
            break;
    }
}
