#!/bin/bash
dotnet tool restore
if [ "$PACKAGE_VERSION" ]; then
    dotnet nuget add source "${CI_API_V4_URL}/projects/51515286/packages/nuget/index.json" --name core
    dotnet nuget add source "${CI_API_V4_URL}/projects/51838363/packages/nuget/index.json" --name scale
    dotnet add package tenduke.scale --version $PACKAGE_VERSION
fi
dotnet csharpier --check .
dotnet build
